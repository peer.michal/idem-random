import pytest

from tests.integration.states import run_sls

esm_cache = {}

STATE_ID_PRESENT = """
random_id:
  random.id.present:
    - name: rp
    - length: 6
"""

STATE_ID_PRESENT_WITH_RESOURCE_ID = """
random_id_with_resource_id:
  random.id.present:
    - name: rp
    - length: 6
    - resource_id: fr493l
"""

STATE_ID_ABSENT = """
random_id:
  random.id.absent:
    - name: rp
"""


def test_id_present(hub):
    running = run_sls(STATE_ID_PRESENT, managed_state=esm_cache)
    ret = running["random.id_|-random_id_|-random_id_|-present"]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["new_state"]
    new_state = ret["new_state"]
    assert new_state
    assert len(new_state["output"]) == 6

    running = run_sls(STATE_ID_PRESENT_WITH_RESOURCE_ID, managed_state=esm_cache)
    ret = running[
        "random.id_|-random_id_with_resource_id_|-random_id_with_resource_id_|-present"
    ]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    new_state = ret["new_state"]
    assert new_state
    assert len(new_state["output"]) == 6
    assert new_state["resource_id"] == "fr493l"


@pytest.mark.depends(on=["test_id_present"])
def test_id_absent(hub):
    running = run_sls(STATE_ID_ABSENT, managed_state=esm_cache)
    ret = running["random.id_|-random_id_|-random_id_|-absent"]
    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["new_state"] is None
    assert len(ret["old_state"]["output"]) == 6

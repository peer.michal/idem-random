import pytest

from tests.integration.states import run_sls

esm_cache = {}

STATE_INT_PRESENT = """
random_integer:
  random.integer.present:
    - name: ri
    - min: 1
    - max: 9
    - seed: 1234
"""

STATE_NEW_INT_PRESENT = """
random_integer:
  random.integer.present:
    - name: new_ri
    - min: 1
    - max: 12
    - seed: 1234
"""

STATE_INT_PRESENT_WITH_RESOURCE_ID = """
random_integer_with_resource_id:
  random.integer.present:
    - name: ri
    - min: 1
    - max: 12
    - seed: 1234
    - resource_id: 10
"""


STATE_INT_ABSENT = """
random_integer:
  random.integer.absent:
    - name: ri
"""


def test_random_integer_present(hub):
    running = run_sls(STATE_INT_PRESENT, managed_state=esm_cache)
    ret = running["random.integer_|-random_integer_|-random_integer_|-present"]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["new_state"]
    new_state = ret["new_state"]
    assert new_state
    output1 = new_state["output"]

    running = run_sls(STATE_NEW_INT_PRESENT, managed_state=esm_cache)
    ret = running["random.integer_|-random_integer_|-random_integer_|-present"]
    assert ret["result"], ret["comment"]
    output2 = ret["new_state"]["output"]
    assert output1 == output2

    running = run_sls(STATE_INT_PRESENT_WITH_RESOURCE_ID, managed_state=esm_cache)
    ret = running[
        "random.integer_|-random_integer_with_resource_id_|-random_integer_with_resource_id_|-present"
    ]
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    new_state = ret["new_state"]
    assert new_state
    assert 10 == new_state["output"]


@pytest.mark.depends(on=["test_integer_present"])
def test_random_integer_absent(hub):
    running = run_sls(STATE_INT_ABSENT, managed_state=esm_cache)
    ret = running["random.integer_|-random_integer_|-random_integer_|-absent"]
    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["new_state"] is None
    assert ret["old_state"]["output"]
